// import logo from './logo.svg';
import './App.css';
import Product from './components/Product';
import Welcome from './components/Welcome';

import { Route, Routes } from 'react-router-dom';
import Header from './components/Header';
import ProductDetail from './components/ProductDetail';

function App() {
  return (
    <div className="App">

      <header>
        <Header/>
      </header>
     

      <Routes>
        <Route path='/' element={<Welcome/>}>
        </Route>
        <Route path='/Product' element={<Product/>}> {/* o exact, faz com que este seja renderizado caso seja o nome exacto do path (ou seja, se aparecer um link com algo dps de "/product" , ele vai ser usado) */}
        
        </Route>
        <Route path='/Product/:productId' element={<ProductDetail/>}> {/* ou meter este em primeiro */}
          
        </Route>
        <Route path='/Welcome' element={<Welcome/>}>
          
        </Route>
      </Routes>
      
    </div>
  );
}

export default App;