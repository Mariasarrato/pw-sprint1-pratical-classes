import React from 'react';
import { Link } from 'react-router-dom';

const Product = () => {
    return (
        <section>
            <h1>Product</h1>
            <ul>
                <p>
                    <Link to='/product/p1'>Garrafa de água</Link>
                </p>
                <p>
                    <Link to='/product/p2'>Comando</Link>
                </p>
                <p>
                    <Link to='/product/p3'>Chave</Link>
                </p>
            </ul>
        </section>
    );
};

export default Product;