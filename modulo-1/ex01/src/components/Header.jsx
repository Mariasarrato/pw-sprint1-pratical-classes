import React from "react";
import { Link } from "react-router-dom";
import Welcome from "./Welcome";

import "./Header.css";

const Header = () => {
  return (
    
    <header>
      <div className="header">
        <div className="btn_header">
          <Link to="/Welcome" className="link">
            Welcome {/* txt que vai aparecer no link */}
          </Link>
        </div>
        <div className="btn_header">
          <Link to="/Product" className="link">
            Product {/* txt que vai aparecer no link */}
          </Link>
        </div>
      </div>

    
    </header>
  );
};

export default Header;
